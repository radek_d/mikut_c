﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BillingReader;

namespace CostCalculator
{
    public class WithoutFreePackage : IParameterStrategy
    {
        public decimal Compute(Tariff tariff, IOperationWithOperator operation)
        {
            decimal cost = 0;
            OperationType type = operation.GetOperationType();

            OperatorPricing operatorPricing = null;
            //tariff.GetData(operation.GetOperator(), out operatorPricing, out freePackage);
            operatorPricing = tariff.getOperatorPricing(operation.GetOperator());

            if (type == OperationType.Call)
            {
                int duration = operation.GetDuration();
                cost += (Math.Ceiling(((decimal)duration) / tariff.UnitTimeInSeconds) * operatorPricing.UnitCost);
            }
            else if (type == OperationType.SMS)
            {
                cost += operatorPricing.SMSCost;
            }

            return cost;
        }
    }
}

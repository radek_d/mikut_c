﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CostCalculator
{
    public interface IParameterStrategy
    {
        decimal Compute(Tariff tariff, IOperationWithOperator operation);
    }
}

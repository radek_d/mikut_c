﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BillingReader;
using MiKUTModel;


namespace CostCalculator
{
    public interface IOperationWithOperator : IOperation 
    {
        Operator GetOperator(); 
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CostCalculator
{
    public class FreePackage
    {
        private int _freeSeconds;
        private int _freeSMS;
        private int _secondsToSMS;

        //-----------------------------------
        public int FreeSeconds
        {
            get { return _freeSeconds; }
            set { _freeSeconds = value; }
        }

        public int FreeSMS
        {
            get { return _freeSMS; }
            set { _freeSMS = value; }
        }

        public int SecondsToSMS
        {
            get { return _secondsToSMS; }
            set { _secondsToSMS = value; }
        }

        //-----------------------------------
        public FreePackage(int freeSeconds, int freeSMS, int secondsToSMS)
        {
            _freeSeconds = freeSeconds;
            _freeSMS = freeSMS;
            _secondsToSMS = secondsToSMS;
        }
    }
}

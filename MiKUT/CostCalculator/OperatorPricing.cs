﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MiKUTModel;

namespace CostCalculator
{
    public class OperatorPricing
    {
        private Operator _operator;
        private int _groupID;
        private decimal _unitCost;
        private decimal _SMSCost;

        //-----------------------------------
        public Operator Operator
        {
            get { return _operator; }
            set { _operator = value; }
        }

        public int GroupID
        {
            get { return _groupID; }
            set { _groupID = value; }
        }

        public decimal UnitCost
        {
            get { return _unitCost; }
            set { _unitCost = value; }
        }

        public decimal SMSCost
        {
            get { return _SMSCost; }
            set { _SMSCost = value; }
        }

        //-----------------------------------
        public OperatorPricing(Operator mobileOperator, int groupID, decimal unitCost, decimal smsCost)
        {
            _operator = mobileOperator;
            _groupID = groupID;
            _unitCost = unitCost;
            _SMSCost = smsCost;
        }
    }
}

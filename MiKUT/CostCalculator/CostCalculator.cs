﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CostCalculator
{
    public class CostCalculator : ICalculator
    {
        public decimal Calculate(Tariff tariff, IOperationWithOperator[] billing)
        {
            Tariff t = new Tariff(tariff);
            decimal cost = t.FixedMonthlyCost;

            foreach (IOperationWithOperator iowo in billing)
            {
                cost += CreateParameterStrategy(t, iowo).Compute(t, iowo);
            }

            return cost;
        }

        public virtual IParameterStrategy CreateParameterStrategy(Tariff tariff, IOperationWithOperator operation)
        {
            MiKUTModel.Operator mobileOperator = operation.GetOperator();
            OperatorPricing operatorPricing = null;
            FreePackage freePackage = null;

            tariff.GetData(mobileOperator, out operatorPricing, out freePackage);

            if (freePackage == null || (freePackage.FreeSeconds == 0 && freePackage.FreeSMS == 0))
                return new WithoutFreePackage();
            else if (freePackage.FreeSeconds == 0 && freePackage.FreeSMS > 0)
                return new OnlyFreeSMS();
            else if (freePackage.FreeSeconds > 0 && freePackage.FreeSMS > 0)
                return new FreeUnitsAndSMS();
            else if (freePackage.FreeSeconds > 0 && freePackage.FreeSMS == 0)
            {
                if (freePackage.SecondsToSMS > 0)
                    return new OnlyFreeUnitsWithChange();
                else if (freePackage.SecondsToSMS == 0)   //gdy == 0 to oznacza ze nie mozna wymienic minut na smsy
                    return new OnlyFreeUnitsWithoutChange();
            }
            else
                throw new Exception("Blad! Nie odnaleziono odpowiedniej strategii obliczenia kosztu dla pewnej operacji z billingu!");

            return null;
        }
    }
}
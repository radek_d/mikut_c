﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MiKUTModel;

namespace CostCalculator
{
    public class Tariff
    {
        private string _name;
        private decimal _fixedMonthlyCost;
        private List<OperatorPricing> _pricing;
        private int _unitTimeInSeconds;
        private Dictionary<int, FreePackage> _freePackagesGroup;

        //-----------------------------------
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public decimal FixedMonthlyCost 
        {
            get { return _fixedMonthlyCost; }
            set { _fixedMonthlyCost = value; }
        }

        public IEnumerable<OperatorPricing> Pricing
        {
            get { return _pricing; }
        }

        public int UnitTimeInSeconds
        {
            get { return _unitTimeInSeconds; }
            set { _unitTimeInSeconds = value; }
        }

        public IEnumerable<KeyValuePair<int, FreePackage>> FreePackagesGroup
        {
            get { return _freePackagesGroup; }
        }

        //-----------------------------------
        public Tariff(decimal fixedMonthlyCost, int unitTimeInSeconds)
        {
            _fixedMonthlyCost = fixedMonthlyCost;
            _pricing = new List<OperatorPricing>();
            _unitTimeInSeconds = unitTimeInSeconds;
            _freePackagesGroup = new Dictionary<int, FreePackage>();
        }

        public Tariff(Tariff tariff)
        {
            _name = tariff.Name;
            _fixedMonthlyCost = tariff.FixedMonthlyCost;
            _pricing = new List<OperatorPricing>();
            foreach(OperatorPricing op in tariff.Pricing)
                AddPricing(op);

            _unitTimeInSeconds = tariff.UnitTimeInSeconds;
            _freePackagesGroup = new Dictionary<int, FreePackage>();
            foreach (KeyValuePair<int, FreePackage> fpg in tariff.FreePackagesGroup)
                AddFreePackageGroup(fpg.Key, fpg.Value);
        }

        public void AddPricing(OperatorPricing operatorPricing)
        {
            _pricing.Add(operatorPricing);
        }

        public void AddFreePackageGroup(int groupID, FreePackage freePackage)
        {
            _freePackagesGroup.Add(groupID, freePackage);
        }

        public void GetData(Operator mobileOperator, out OperatorPricing operatorPricing, out FreePackage freePackage)
        {
            operatorPricing = null;
            foreach(OperatorPricing op in _pricing)
                if(op.Operator.Equals(mobileOperator))
                    if(operatorPricing == null)
                        operatorPricing = op;
                    else
                        throw new Exception(String.Format("Blad! Operator {0} wystepuje co najmniej 2 razy w cenniku taryfy!", mobileOperator));

            if (operatorPricing == null)
                throw new Exception(String.Format("Blad! Nie znaleziono operatora {0} w cenniku taryfy!", mobileOperator));

            freePackage = null;
            foreach (KeyValuePair<int, FreePackage> fpg in FreePackagesGroup)
                if (fpg.Key == operatorPricing.GroupID)
                    freePackage = fpg.Value;
        }

        public OperatorPricing getOperatorPricing(Operator mobileOperator)
        {
            OperatorPricing operatorPricing = null;
            foreach (OperatorPricing op in _pricing)
                if (op.Operator.Equals(mobileOperator))
                {
                    if (operatorPricing == null)
                        operatorPricing = op;
                    else
                        throw new Exception(String.Format("Blad! Operator {0} wystepuje co najmniej 2 razy w cenniku taryfy!", mobileOperator));
                }

            if (operatorPricing == null)
                throw new Exception(String.Format("Blad! Nie znaleziono operatora {0} w cenniku taryfy!", mobileOperator));
            
            return operatorPricing;
        }

        public FreePackage getFreePackage(Operator mobileOperator)
        {
            FreePackage freePackage = null;
            OperatorPricing operatorPricing = getOperatorPricing(mobileOperator);
            foreach (KeyValuePair<int, FreePackage> fpg in FreePackagesGroup)
                if (fpg.Key == operatorPricing.GroupID)
                    freePackage = fpg.Value;

            return freePackage;
        }
    }
}

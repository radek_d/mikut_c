﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BillingReader;

namespace CostCalculator
{
    public class FreeUnitsAndSMS : IParameterStrategy
    {
        public decimal Compute(Tariff tariff, IOperationWithOperator operation)
        {
            decimal cost = 0;
            OperationType type = operation.GetOperationType();

            OperatorPricing operatorPricing = null;
            FreePackage freePackage = null;
            //tariff.GetData(operation.GetOperator(), out operatorPricing, out freePackage);
            operatorPricing = tariff.getOperatorPricing(operation.GetOperator());
            freePackage = tariff.getFreePackage(operation.GetOperator());

            if (type == OperationType.Call)
            {
                int duration = operation.GetDuration();
                if (duration > freePackage.FreeSeconds)
                {
                    duration -= freePackage.FreeSeconds;
                    freePackage.FreeSeconds = 0;
                    cost += Math.Ceiling(((decimal)duration) / tariff.UnitTimeInSeconds) * operatorPricing.UnitCost;
                }
                else
                    freePackage.FreeSeconds -= duration;
            }
            else if (type == OperationType.SMS)
            {
                freePackage.FreeSMS--;  //bo przy wyborze strategii wiadomo ze jest co najmniej 1 darmowy sms
            }

            return cost;
        }
    }
}

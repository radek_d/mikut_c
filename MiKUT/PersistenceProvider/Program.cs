﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MiKUTModel;

namespace PersistenceProvider
{
    class Program
    {
        static void Main(string[] args)
        {
            // Sample NumberDAO usage
            NumberDAO dao = new NumberDAO();
            dao.bindNumberWithOperator("123", Operator.TMobile);
            Console.WriteLine("bind:" + dao.getOperatorForNumber("123"));
            dao.removeNumber("123");
            Console.WriteLine("removed:" + dao.getOperatorForNumber("123"));
            Console.ReadKey();

        }
    }
}

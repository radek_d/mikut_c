﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Windows.Forms;
using System.Data;
using CostCalculator;
namespace PersistenceProvider
{
    public class TariffDAO:ITariffDAO
    {
        private SQLiteDatabase db;

        public TariffDAO()
        {
            db = new SQLiteDatabase();
        }

        public void addTariff(Tariff tariff){
            
            //Dodanie Tariff potrzebna czegos unikalnego by miec id :/
            Dictionary<String, String> tarifa = new Dictionary<String, String>();
            tarifa.Add("Name", Convert.ToString(tariff.Name));
            tarifa.Add("fixedMonthlyCost", Convert.ToString(tariff.FixedMonthlyCost));
            tarifa.Add("UnitTimeInSeconds",Convert.ToString(tariff.UnitTimeInSeconds));

            db.Insert("Tariff", tarifa);



                //Dodanie FreePackage
            string TariffID = db.ExecuteScalar("Select Count(TariffID) from Tariff");
            IEnumerable<KeyValuePair<int, FreePackage>> Packages = tariff.FreePackagesGroup;
            Dictionary<int, FreePackage> freePackage = Packages.ToDictionary(x => x.Key, x => x.Value);
            foreach (FreePackage liscie in freePackage.Values)
            {
                Dictionary<String, String> FreePackage = new Dictionary<String, String>();
                FreePackage.Add("TariffID", TariffID);
                FreePackage.Add("FreeSMS", Convert.ToString(liscie.FreeSMS));
                FreePackage.Add("FreeSeconds", Convert.ToString(liscie.FreeSeconds));
                FreePackage.Add("Second_toSMS", Convert.ToString(liscie.SecondsToSMS));
                db.Insert("FreePackage", FreePackage);
            }

            //Dodanie Operator pricing

            Dictionary<String, String> OperatorPrincing = new Dictionary<String, String>();
            IEnumerable<OperatorPricing> Prices = tariff.Pricing;
            List<OperatorPricing> Lista = Prices.ToList();
            foreach (OperatorPricing lisc in Lista)
            {
                OperatorPrincing.Add("TariffID", TariffID); 
                OperatorPrincing.Add("SMSCost", Convert.ToString(lisc.SMSCost));
                OperatorPrincing.Add("UnitCost", Convert.ToString(lisc.UnitCost));
                OperatorPrincing.Add("groupID", Convert.ToString(lisc.GroupID));

                db.Insert("OperatorPrincing", OperatorPrincing);
            }

        }


        public List<Tariff> getAllTarifes(){
            String query="Select Tarifs  \"TARIFS\" from Operator;";
            DataTable recipe = db.GetDataTable(query);

            return null;
        }

        //usuwanie Taryfy
        public void removeTarife(Tariff tariff)
        { 
            string napis=("SELECT TariffID FROM Tariff where Name =" + tariff.Name + ";");
            string TariffID = db.ExecuteScalar(napis);
            db.Delete("Tariff", String.Format("TariffID = {0}", tariff));
            db.Delete("OperatorPrincing", String.Format("TariffID = {0}", tariff));
            db.Delete("FreePackage", String.Format("TariffID = {0}", tariff));
        }

    } 
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MiKUTModel;

namespace PersistenceProvider
{
    interface INumberDAO
    {
        void bindNumberWithOperator(String number, Operator op);
        void removeNumber(String number);
        Operator getOperatorForNumber(String number);
    }
}

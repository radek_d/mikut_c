﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CostCalculator;

namespace PersistenceProvider
{
    public interface ITariffDAO
    {
        void addTariff(Tariff tariff);
        List<Tariff> getAllTarifes();
        void removeTarife(Tariff tariff);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersistenceProvider
{
    class TabelsDefinition
    {
        public const String NUMBERS = "CREATE TABLE IF NOT EXISTS \"numbers\" (\"id\" INTEGER PRIMARY KEY  NOT NULL , \"number\" VARCHAR, \"operator_id\" INTEGER)";
        public const String TARIFFS = "CREATE TABLE IF NOT EXISTS \"tariffs\" (\"id\" INTEGER PRIMARY KEY  NOT NULL , \"fixedMonthlyCost\" FLOAT, \"unitTimeInSeconds\" INTEGER, \"operator_id\" INTEGER)";
        public const String FREE_PACKAGES = "CREATE TABLE IF NOT EXISTS \"free_packages\" (\"id\" INTEGER PRIMARY KEY  NOT NULL , \"freeSMS\" INTEGER, \"freeSeconds\" INTEGER, \"secondsToSms\" INTEGER, \"tariff_id\" INTEGER)";
        public const String OPERATOR_PRICINGS = "CREATE TABLE IF NOT EXISTS \"operator_pricings\" (\"id\" INTEGER PRIMARY KEY  NOT NULL , \"SMSCost\" FLOAT, \"UnitCost\" FLOAT, \"group_id\" INTEGER, \"operator_id\" INTEGER, \"tariff_id\" INTEGER)";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using MiKUTModel;

namespace PersistenceProvider
{

    public class NumberDAO : INumberDAO
    {
        private SQLiteDatabase db;

        public NumberDAO()
        {
            db = new SQLiteDatabase();
        }

        public void bindNumberWithOperator(String number, Operator op)
        {
            Dictionary<String, String> data = new Dictionary<String, String>();
            data.Add("number", number);
            data.Add("operator_id", Convert.ToString((int)op));
            db.Insert("numbers", data);
        }

        public void removeNumber(String number)
        {
            db.Delete("numbers", String.Format("number = {0}", number));
        }

        public Operator getOperatorForNumber(String number)
        {
            String operator_id = db.ExecuteScalar(String.Format("SELECT operator_id FROM numbers WHERE number = '{0}' LIMIT 1;", number));

            if (operator_id != "")
            {
                return (Operator)Convert.ToInt32(operator_id);
            } 
            else 
            {
                return Operator.Unknown;
            }
        }
    }
}

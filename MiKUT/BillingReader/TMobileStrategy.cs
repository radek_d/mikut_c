﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace BillingReader{
    public class TMobileStrategy : OperatorStrategy{
        private String _pattern = @"\d{4}-\d{2}-\d{2}\s+\d{2}:\d{2}\s+(SMS|Telefoniczne)\s+\d{9,11}\s+\d{2}:\d{2}:\d{2}\s+\d+,\d{2} zł\s*";

        public override string GetPattern() {
            return _pattern;
        }

        public override IOperation GetOperation(String line) {
            //replace multiple spaces with 1
            Regex re = new Regex(@"[ ]{2,}");
            line = re.Replace(line, " ");
            
            //split line by spaces
            String[] lineSeparated = line.Split(null);

            //extract data from array
            //number
            String number = lineSeparated[3];

            //type : call or sms
            OperationType type;
            if (lineSeparated[2] == "SMS") type = OperationType.SMS;
            else type = OperationType.Call;

            //datetime
            DateTime time = DateTime.Parse(lineSeparated[0] + " " + lineSeparated[1]);

            //duration
            String[] durationSeparated = lineSeparated[4].Split(':');
            int duration = 0;
            try {
                duration += Convert.ToInt32(durationSeparated[2]);
                duration += Convert.ToInt32(durationSeparated[1]) * 60;
                duration += Convert.ToInt32(durationSeparated[0]) * 360;
            }
            catch (InvalidCastException) { }

            return new Operation(number, type, time, duration); 
        }
    }
}

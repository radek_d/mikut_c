﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections;
using System.Text.RegularExpressions;


namespace BillingReader{
    public class BillingReader : IBillingReader{
        private ArrayList _strategies;

        public IOperation[] LoadBilling(String path) {
            OperatorStrategy strategy = null;
            ArrayList billing = new ArrayList();

            //opening given file
//todo - file exceptions
            System.IO.StreamReader file = new System.IO.StreamReader(path);
            string line;

            //reading file line by line
            while ((line = file.ReadLine()) != null) {
                //select strategy
                if (strategy == null) strategy = GetStrategy(line);
                //if strategy is selected - process line
                if (strategy != null) {
                    IOperation operation = strategy.GetOperation(line);
                    if(operation != null) billing.Add(operation);
                }
            }
//todo - format exceptions

            return (IOperation[])billing.ToArray(typeof(IOperation));
        }


        public OperatorStrategy GetStrategy(String line) {
            if (_strategies == null) {
                _strategies = new ArrayList();
                _strategies.Add(new TMobileStrategy());
                _strategies.Add(new PlayStrategy());
            }

            foreach (OperatorStrategy strategy in _strategies) {
                if (System.Text.RegularExpressions.Regex.IsMatch(line, strategy.GetPattern(), System.Text.RegularExpressions.RegexOptions.IgnoreCase)) {
                    return strategy;
                }
            }

            return null;
        }

    }
}

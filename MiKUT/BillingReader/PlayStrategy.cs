﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillingReader {
    public class PlayStrategy : OperatorStrategy{
        private String _pattern = @"\d+,(Wychodz.ce|Przychodz.ce),(SMS|Rozmowy g.osowe|Dane),\d{4}\.\d{2}.\d{2},\d{2}:\d{2}:\d{2},\d{9,11},.+PLAY.+,";

        public override string GetPattern() {
            return _pattern;
        }

        public override IOperation GetOperation(String line) {
            //split line by commas
            String[] lineSeparated = line.Split(',');

            //extract data from array
            //type - incomming, outcoming
            if (lineSeparated[1].ElementAt(0) == 'P') return null;

            //operation type
            if (lineSeparated[2] == "Dane") return null;

            //number
            String number = lineSeparated[5];

            //type : call or sms
            OperationType type;
            if (lineSeparated[2] == "SMS") type = OperationType.SMS;
            else type = OperationType.Call;

            //datetime
            DateTime time = DateTime.Parse(lineSeparated[3] + " " + lineSeparated[4]);

            //duration
            String[] durationSeparated = lineSeparated[7].Substring(0,4).Split(':');
            int duration = 0;
            if (type == OperationType.Call) {
                try {
                    duration += Convert.ToInt32(durationSeparated[1]);
                    duration += Convert.ToInt32(durationSeparated[0]) * 60;
                }
                catch (InvalidCastException) { }
            }

            return new Operation(number, type, time, duration);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BillingReader {
    public interface IOperation {
        DateTime GetDate();
        int GetDuration();
        String GetNumber();
        OperationType GetOperationType();
    }
}

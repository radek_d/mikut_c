﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillingReader {
    public class Operation : IOperation {
        private DateTime _date;
        private String _number;
        private int _duration;
        private OperationType _type;

        public Operation(String number, OperationType type, DateTime date, int duration) {
            _number = number;
            _type = type;
            _date = date;
            _duration = duration;
        }

        public DateTime GetDate() {
            return _date;
        }
        public int GetDuration() {
            return _duration;
        }
        public String GetNumber() {
            return _number;
        }
        public OperationType GetOperationType() {
            return _type;
        }

        public override String ToString() {
            return _number + " " + _type + " " + _date + " " + _duration;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BillingReader {
    public abstract class OperatorStrategy {
        public abstract String GetPattern();
        public abstract IOperation GetOperation(String line);
    }
}

﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CostOptimizer;
using CostCalculator;
using NMock;
using PersistenceProvider;

namespace MiKUTTests
{
    [TestClass]
    public class CostOptimizerTest
    {
        private readonly MockFactory mockFactory = new MockFactory();

        [TestMethod]
        public void FindOptimalTest()
        {
            var tariffDao = mockFactory.CreateMock<ITariffDAO>();
            var method = mockFactory.CreateMock<Optimizer.ResolveOperator>();
            OptimizerTest optimizer = new OptimizerTest(tariffDao.MockObject, method.MockObject);

            var costCalculator = mockFactory.CreateMock<CostCalculator.CostCalculator>();

            var billing = mockFactory.CreateMock<IOperationWithOperator[]>();
            var tariff1 = mockFactory.CreateMock<Tariff>();
            var tariff2 = mockFactory.CreateMock<Tariff>();
            var tariff3 = mockFactory.CreateMock<Tariff>();
            var tariff4 = mockFactory.CreateMock<Tariff>();
            
            costCalculator.Expects.One.MethodWith(_ => _.Calculate(tariff1.MockObject, billing.MockObject)).WillReturn(50);
            costCalculator.Expects.One.MethodWith(_ => _.Calculate(tariff2.MockObject, billing.MockObject)).WillReturn(57);
            costCalculator.Expects.One.MethodWith(_ => _.Calculate(tariff3.MockObject, billing.MockObject)).WillReturn(40);
            costCalculator.Expects.One.MethodWith(_ => _.Calculate(tariff4.MockObject, billing.MockObject)).WillReturn(53);
            
            Tariff[] tariffList = new Tariff[4];
            tariffList[0] = tariff1.MockObject;
            tariffList[1] = tariff2.MockObject;
            tariffList[2] = tariff3.MockObject;
            tariffList[3] = tariff4.MockObject;
            
            Assert.AreEqual(tariff3.MockObject, optimizer.CalculateCostAndFindOptimalTest(tariffList, billing.MockObject));
        }

        [TestMethod, Timeout(15000)]
        public void UptimeTest()
        {
            var tariffDao = mockFactory.CreateMock<ITariffDAO>();
            var method = mockFactory.CreateMock<Optimizer.ResolveOperator>();
            const string pathToBilling = "C:";  //billing example
            Optimizer optimizer = new Optimizer(tariffDao.MockObject, method.MockObject);
            optimizer.FindOptimalTariff(pathToBilling);
        }
    }
}


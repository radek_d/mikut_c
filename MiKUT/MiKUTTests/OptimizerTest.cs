﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CostCalculator;
using PersistenceProvider;

namespace CostOptimizer
{
    public class OptimizerTest : Optimizer {
        
        public OptimizerTest(ITariffDAO tariffDao, ResolveOperator method) : base(tariffDao, method)
        {  
        }

        public Tariff CalculateCostAndFindOptimalTest(Tariff[] tariff, IOperationWithOperator[] billing)
        {
            return base.CalculateCostAndFindOptimal(tariff, billing);
        }
    }
}

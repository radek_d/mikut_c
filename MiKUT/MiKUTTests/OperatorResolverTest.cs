using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OperatorResolver;
using MiKUTModel;
using NMock;

namespace MiKUTTests
{



    [TestClass]
    public class OperatorResolverTest
    {
        private readonly MockFactory mockFactory = new MockFactory();

        public static ResolveMethod WWWMethodObject;
        public static ResolveMethod PersistenceMethodObject;
        public static ResolveMethod FileMethodObject;


        [TestInitialize]
        public void SetUp()
        {
            var wwwMethod = mockFactory.CreateMock<OperatorResolver.WWWMethod>();
            var persistenceMethod = mockFactory.CreateMock<OperatorResolver.PersistenceMethod>();
            var fileMethod = mockFactory.CreateMock<OperatorResolver.FileMethod>();

            WWWMethodObject = wwwMethod.MockObject;
            PersistenceMethodObject = persistenceMethod.MockObject;
            FileMethodObject = fileMethod.MockObject;

            wwwMethod.Expects.One.MethodWith(_ => _.SpecResolve("1")).WillReturn(Operator.Plus);
            wwwMethod.Expects.One.MethodWith(_ => _.SpecResolve("2")).WillReturn(Operator.Unknown);
            wwwMethod.Expects.One.MethodWith(_ => _.SpecResolve("3")).WillReturn(Operator.Unknown);


            persistenceMethod.Expects.One.MethodWith(_ => _.SpecResolve("2")).WillReturn(Operator.Play);
            persistenceMethod.Expects.One.MethodWith(_ => _.SpecResolve("1")).WillReturn(Operator.Unknown);
            persistenceMethod.Expects.One.MethodWith(_ => _.SpecResolve("3")).WillReturn(Operator.Unknown);

            fileMethod.Expects.One.MethodWith(_ => _.SpecResolve("3")).WillReturn(Operator.Orange);
            fileMethod.Expects.One.MethodWith(_ => _.SpecResolve("2")).WillReturn(Operator.Unknown);
            fileMethod.Expects.One.MethodWith(_ => _.SpecResolve("1")).WillReturn(Operator.Unknown);


        }



        [TestMethod]
        public void ChainTest()
        {



            OperatorResolver.IOperatorResolver Op = new MyOperatorResolver();

            Assert.AreEqual(Operator.Plus, Op.Resolve("1"));
            Assert.AreEqual(Operator.Play, Op.Resolve("2"));
            Assert.AreEqual(Operator.Orange, Op.Resolve("3"));

        }

        [TestMethod]
        public void WWWTest()
        {
            ResolveMethod www = new WWWMethod();
            Assert.AreEqual(Operator.Play, www.SpecResolve("604363669"));
        }

        [TestMethod]
        public void FileTest()
        {
            ResolveMethod file = new FileMethod();
            Assert.AreEqual(Operator.Play, file.SpecResolve("518185974"));

        }

        [TestMethod]
        public void PersistenceTest()
        {
            throw new NotImplementedException();
        }


    }


    public class MyOperatorResolver : OperatorResolver.OperatorResolver
    {
       
        
        public override ResolveMethod GetMethod(int num)
        {
            switch (num)
            {
                case 0:
                    return OperatorResolverTest.PersistenceMethodObject;
                case 1:
                    return OperatorResolverTest.WWWMethodObject;
                case 2:
                    return OperatorResolverTest.FileMethodObject;
                default:
                    return base.GetMethod(num);
            }
        }
    }

}
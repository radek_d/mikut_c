﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PersistenceProvider;

namespace MiKUTTests
{
    [TestClass]
    public class NumberDAOTest
    {
        [TestMethod]
        public void itBindsNumberWithOperator()
        {
            NumberDAO dao = new NumberDAO();
            dao.bindNumberWithOperator("123", Operator.Orange);
            Assert.AreEqual(Operator.Orange, dao.getOperatorForNumber("123"));
        }

        [TestMethod]
        public void itReturnUnknowIfNoNumberFound()
        {
            NumberDAO dao = new NumberDAO();
            Assert.AreEqual(Operator.Unknown, dao.getOperatorForNumber("123456789"));
        }

        [TestMethod]
        public void ItReturnsUnkonwnIfNumberDeleted()
        {
            NumberDAO dao = new NumberDAO();
            dao.bindNumberWithOperator("123", Operator.Orange);
            dao.removeNumber("123");
            Assert.AreEqual(Operator.Unknown, dao.getOperatorForNumber("123"));
        }
    }
}

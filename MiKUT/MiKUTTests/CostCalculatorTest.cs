﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NMock;
using CostCalculator;
using OperatorResolver;
using CostOptimizer;
using BillingReader;
using MiKUTModel;

namespace MiKUTTests
{
    [TestClass]
    public class CostCalculatorTest
    {
        private readonly MockFactory mockFactory = new MockFactory();

        [TestMethod]
        public void CalculateTest()
        {
            decimal correctResult = 20.5m;

            CostCalculator.CostCalculator costCalculator = new CostCalculator.CostCalculator();
            Tariff tariff = new Tariff(20, 60);

            OperatorPricing operatorPricing1 = new OperatorPricing(Operator.TMobile, 1, 0.25m, 0.20m);
            OperatorPricing operatorPricing2 = new OperatorPricing(Operator.Play, 2, 0.5m, 0.25m);
            tariff.AddPricing(operatorPricing1);
            tariff.AddPricing(operatorPricing2);

            IOperationWithOperator[] billing = new OperationWithOperator[2];
            billing[0] = new OperationWithOperator(new Operation("1234", OperationType.Call, new DateTime(), 60), Operator.TMobile);
            billing[1] = new OperationWithOperator(new Operation("4321", OperationType.SMS, new DateTime(), 0), Operator.Play);

            Assert.AreEqual(costCalculator.Calculate(tariff, billing), correctResult);
        }


        [TestMethod]
        public void FreeUnitsAndSMSTest()
        {
            decimal correctResult = 0.3m;

            var operation = mockFactory.CreateMock<OperationWithOperator>();
            var tariff = mockFactory.CreateMock<Tariff>();
            var operatorPricing = mockFactory.CreateMock<OperatorPricing>();
            var freePackage = mockFactory.CreateMock<FreePackage>();

            tariff.Expects.One.MethodWith(_ => _.getOperatorPricing(Operator.Orange)).WillReturn(operatorPricing.MockObject);
            tariff.Expects.One.MethodWith(_ => _.getFreePackage(Operator.Orange)).WillReturn(freePackage.MockObject);
            tariff.Expects.One.MethodWith(_ => _.UnitTimeInSeconds).WillReturn(60);
            operation.Expects.One.MethodWith(_ => _.GetOperationType()).WillReturn(OperationType.Call);
            operation.Expects.One.MethodWith(_ => _.GetOperator()).WillReturn(Operator.Orange);
            operation.Expects.One.MethodWith(_ => _.GetDuration()).WillReturn(60);
            operatorPricing.Expects.One.MethodWith(_ => _.UnitCost).WillReturn(0.36m);
            freePackage.Expects.One.SetProperty<int>(_ => _.FreeSeconds);
            freePackage.Expects.One.SetProperty<int>(_ => _.FreeSMS);
            freePackage.Expects.One.MethodWith(_ => _.FreeSeconds).WillReturn(55);
            freePackage.Expects.One.MethodWith(_ => _.FreeSMS).WillReturn(1);

            FreeUnitsAndSMS freeUnitsAndSMS = new FreeUnitsAndSMS();

            Assert.AreEqual(freeUnitsAndSMS.Compute(tariff.MockObject, operation.MockObject), correctResult);
        }

        [TestMethod]
        public void OnlyFreeSMSTest()
        {
            decimal correctResult = 0.0m;

            var operation = mockFactory.CreateMock<OperationWithOperator>();
            var tariff = mockFactory.CreateMock<Tariff>();
            var operatorPricing = mockFactory.CreateMock<OperatorPricing>();
            var freePackage = mockFactory.CreateMock<FreePackage>();

            tariff.Expects.One.MethodWith(_ => _.getOperatorPricing(Operator.Orange)).WillReturn(operatorPricing.MockObject);
            tariff.Expects.One.MethodWith(_ => _.getFreePackage(Operator.Orange)).WillReturn(freePackage.MockObject);
            tariff.Expects.One.MethodWith(_ => _.UnitTimeInSeconds).WillReturn(60);
            operation.Expects.One.MethodWith(_ => _.GetOperationType()).WillReturn(OperationType.SMS);
            operation.Expects.One.MethodWith(_ => _.GetOperator()).WillReturn(Operator.Orange);
            operation.Expects.One.MethodWith(_ => _.GetDuration()).WillReturn(60);
            operatorPricing.Expects.One.MethodWith(_ => _.UnitCost).WillReturn(0.36m);
            freePackage.Expects.One.SetProperty<int>(_ => _.FreeSeconds);
            freePackage.Expects.One.SetProperty<int>(_ => _.FreeSMS);
            freePackage.Expects.One.MethodWith(_ => _.FreeSeconds).WillReturn(55);
            freePackage.Expects.One.MethodWith(_ => _.FreeSMS).WillReturn(1);

            OnlyFreeSMS onlyFreeSMS = new OnlyFreeSMS();

            Assert.AreEqual(onlyFreeSMS.Compute(tariff.MockObject, operation.MockObject), correctResult);
        }

        [TestMethod]
        public void OnlyFreeUnitsWithChangeTest()
        {
            decimal correctResult = 0.0m;

            var operation = mockFactory.CreateMock<OperationWithOperator>();
            var tariff = mockFactory.CreateMock<Tariff>();
            var operatorPricing = mockFactory.CreateMock<OperatorPricing>();
            var freePackage = mockFactory.CreateMock<FreePackage>();

            tariff.Expects.One.MethodWith(_ => _.getOperatorPricing(Operator.Orange)).WillReturn(operatorPricing.MockObject);
            tariff.Expects.One.MethodWith(_ => _.getFreePackage(Operator.Orange)).WillReturn(freePackage.MockObject);
            tariff.Expects.One.MethodWith(_ => _.UnitTimeInSeconds).WillReturn(60);
            operation.Expects.One.MethodWith(_ => _.GetOperationType()).WillReturn(OperationType.SMS);
            operation.Expects.One.MethodWith(_ => _.GetOperator()).WillReturn(Operator.Orange);
            operation.Expects.One.MethodWith(_ => _.GetDuration()).WillReturn(60);
            operatorPricing.Expects.One.MethodWith(_ => _.UnitCost).WillReturn(0.36m);
            freePackage.Expects.One.SetProperty<int>(_ => _.FreeSeconds);
            freePackage.Expects.One.SetProperty<int>(_ => _.FreeSMS);
            freePackage.Expects.One.MethodWith(_ => _.SecondsToSMS).WillReturn(50); // 50 sekund pobieramy z darmowych min / sms
            freePackage.Expects.One.MethodWith(_ => _.FreeSeconds).WillReturn(55);  //mamy 55 darmowych sekund
            freePackage.Expects.One.MethodWith(_ => _.FreeSMS).WillReturn(0);

            OnlyFreeUnitsWithChange onlyFreeUnitsWithChange = new OnlyFreeUnitsWithChange();

            Assert.AreEqual(onlyFreeUnitsWithChange.Compute(tariff.MockObject, operation.MockObject), correctResult);
        }

        [TestMethod]
        public void OnlyFreeUnitsWithOutChangeTest()
        {
            decimal correctResult = 0.22m;

            var operation = mockFactory.CreateMock<OperationWithOperator>();
            var tariff = mockFactory.CreateMock<Tariff>();
            var operatorPricing = mockFactory.CreateMock<OperatorPricing>();
            var freePackage = mockFactory.CreateMock<FreePackage>();

            tariff.Expects.One.MethodWith(_ => _.getOperatorPricing(Operator.Orange)).WillReturn(operatorPricing.MockObject);
            tariff.Expects.One.MethodWith(_ => _.getFreePackage(Operator.Orange)).WillReturn(freePackage.MockObject);
            tariff.Expects.One.MethodWith(_ => _.UnitTimeInSeconds).WillReturn(60);
            operation.Expects.One.MethodWith(_ => _.GetOperationType()).WillReturn(OperationType.SMS);
            operation.Expects.One.MethodWith(_ => _.GetOperator()).WillReturn(Operator.Orange);
            operation.Expects.One.MethodWith(_ => _.GetDuration()).WillReturn(60);
            operatorPricing.Expects.One.MethodWith(_ => _.UnitCost).WillReturn(0.36m);
            operatorPricing.Expects.One.MethodWith(_ => _.SMSCost).WillReturn(0.22m);
            freePackage.Expects.One.SetProperty<int>(_ => _.FreeSeconds);
            freePackage.Expects.One.SetProperty<int>(_ => _.FreeSMS);
            freePackage.Expects.One.MethodWith(_ => _.SecondsToSMS).WillReturn(0); // z wymiany sekund na sms nie mozemy skorzystac
            freePackage.Expects.One.MethodWith(_ => _.FreeSeconds).WillReturn(55);  //mamy 55 darmowych sekund
            freePackage.Expects.One.MethodWith(_ => _.FreeSMS).WillReturn(0);

            OnlyFreeUnitsWithoutChange onlyFreeUnitsWithoutChange = new OnlyFreeUnitsWithoutChange();

            Assert.AreEqual(onlyFreeUnitsWithoutChange.Compute(tariff.MockObject, operation.MockObject), correctResult);
        }

        [TestMethod]
        public void WithoutFreePackageTest()
        {
            decimal correctResult = 6.12m;

            var operation = mockFactory.CreateMock<OperationWithOperator>();
            var tariff = mockFactory.CreateMock<Tariff>();
            var operatorPricing = mockFactory.CreateMock<OperatorPricing>();

            tariff.Expects.One.MethodWith(_ => _.getOperatorPricing(Operator.Orange)).WillReturn(operatorPricing.MockObject);
            tariff.Expects.One.MethodWith(_ => _.UnitTimeInSeconds).WillReturn(60);
            operation.Expects.One.MethodWith(_ => _.GetOperationType()).WillReturn(OperationType.Call);
            operation.Expects.One.MethodWith(_ => _.GetOperator()).WillReturn(Operator.Orange);
            operation.Expects.One.MethodWith(_ => _.GetDuration()).WillReturn(1020); //17 units
            operatorPricing.Expects.One.MethodWith(_ => _.UnitCost).WillReturn(0.36m);

            OnlyFreeUnitsWithChange onlyFreeUnitsWithChange = new OnlyFreeUnitsWithChange();

            Assert.AreEqual(onlyFreeUnitsWithChange.Compute(tariff.MockObject, operation.MockObject), correctResult);
        }
    }
}


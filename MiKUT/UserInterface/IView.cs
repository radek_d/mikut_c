﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterface
{
    public interface IView
    {
        void showResult();
        void showNewTarrifWizard();
        void showTarrifsForm();
        void showChooseOperatorBox();
        void showBillingFileBrowser();
        void showProblemBox();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MiKUTModel;


namespace OperatorResolver
{
    abstract public class ResolveMethod
    {
        internal ResolveMethod next;

        internal Operator Resolve(String number)
        {
            Operator toRet = this.SpecResolve(number);
            if (toRet == Operator.Unknown && next != null)
            {
                return next.Resolve(number);
            }
            return toRet;
        }

        public abstract Operator SpecResolve(String number);
    }
}

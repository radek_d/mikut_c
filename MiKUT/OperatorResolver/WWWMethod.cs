﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using MiKUTModel;
using System.Net;
using System.IO;


namespace OperatorResolver
{
    public class WWWMethod:ResolveMethod
    {
       public WWWMethod() { }


       public override Operator SpecResolve(string Number)
       {
           HtmlWeb web = new HtmlWeb();
           web.PreRequest = delegate(HttpWebRequest webRequest)
           {
               webRequest.Timeout = 15000;
               return true;
           };
           HtmlAgilityPack.HtmlDocument Document = new HtmlAgilityPack.HtmlDocument();

           Document = web.Load("http://download.t-mobile.pl/updir/updir.cgi?msisdn=48" + Number);
          


           var node = Document.DocumentNode.SelectSingleNode("//body//table//tbody//table[@align='center']");
           var info = node.SelectNodes("//td");

           String resolvedOperator = null;

           foreach (HtmlNode n in info)
           {

               if (n.InnerText == "Kod sieci:")
               {
                   resolvedOperator = n.NextSibling.InnerText;
               }
           }

           
           switch (resolvedOperator)
           {
               case "260 01":
                   return Operator.Plus;
               case "260 02":
                   return Operator.TMobile;
               case "260 03":
                   return Operator.Orange;
               case "260 06":
                   return Operator.Play;
               default:
                   return Operator.Unknown;
           }
           
       }
    }
}

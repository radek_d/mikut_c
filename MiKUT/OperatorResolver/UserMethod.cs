using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MiKUTModel;


namespace OperatorResolver
{
    
    public delegate Operator UIDelegate(String Num);

    public class UserMethod:ResolveMethod
    {
        private UIDelegate uiDelegate;

        public UserMethod(UIDelegate deleg)
        { 
            this.uiDelegate = deleg;
        }

        public override Operator SpecResolve(string Number)
        {
            return uiDelegate(Number);
        }
    }
}

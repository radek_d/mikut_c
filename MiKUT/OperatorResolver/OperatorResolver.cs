﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MiKUTModel;


namespace OperatorResolver
{
    public class OperatorResolver:IOperatorResolver //Test w projekcie MiKUTTEST
    {
        private ResolveMethod ResponsibilityChain;

        public OperatorResolver()
        {
            GenerateChain();
        }

        public UIDelegate UiDelegate;
 
        public UIDelegate SetUIDelegate(UIDelegate dele){
           return UiDelegate = dele;
        }

        internal void GenerateChain()   //Metoda ta generuje lancuch, ustawia nexty, obiekty strategii pobiera z GetMethod
        {
            ResponsibilityChain = GetMethod(0);
            ResolveMethod Method = ResponsibilityChain;

            int i = 1;
            while (Method != null)
            {
                Method.next = GetMethod(i++);
                Method = Method.next;
            }
             
        }


        public virtual ResolveMethod GetMethod(int num){
            switch (num)
            {
                case 0:
                    return new PersistenceMethod();
                case 1:
                    return new WWWMethod();
                case 2:
                    return new FileMethod();
                case 3:
                    return new UserMethod(UiDelegate);
                default:
                    return null;
            }
        }


        public Operator Resolve(string Number)
        {
            return ResponsibilityChain.Resolve(Number);
        }

        public IDictionary<String, Operator> ResolveMany(IEnumerable<string> Numbers)
        {
            Dictionary<String, Operator> Dict = new Dictionary<string,Operator>();
            foreach (String Num in Numbers)
            {
                Dict.Add(Num, ResponsibilityChain.Resolve(Num));
            }
            return Dict;
        }
    }
}

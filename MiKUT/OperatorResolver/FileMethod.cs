﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using MiKUTModel;


namespace OperatorResolver
{
    public class FileMethod:ResolveMethod
    {
        
        public FileMethod() {}

        private Operator GetOperatorFromFile(string number)
        {
            StreamReader streamReader = File.OpenText("MyOperatorFits.txt");
            string read = null;
            Operator op = Operator.Unknown;
            while ((read = streamReader.ReadLine()) != null)
            {
                string[] result = read.Split(':');
                if (result[0] == number)
                {
                    switch (result[1].ToLower())
                    {
                        case "orange":
                            op = Operator.Orange;
                            break;
                        case "plus":
                            op = Operator.Plus;
                            break;
                        case "play":
                            op = Operator.Play;
                            break;
                        case "tmobile":
                            op = Operator.TMobile;
                            break;
                        case "t-mobile":
                            op = Operator.TMobile;
                            break;
                    }
                    break;
                }
            }
            streamReader.Close();
            return op;
        }

        public override Operator SpecResolve(string number)
        {
            return GetOperatorFromFile(number);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MiKUTModel;


namespace OperatorResolver
{
    public interface IOperatorResolver
    {
        Operator Resolve(String Number);
        IDictionary<String, Operator> ResolveMany(IEnumerable<string> Numbers);
 
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OperatorResolver;
using BillingReader;
using CostCalculator;
using MiKUTModel;

namespace CostOptimizer
{
    public class OperationWithOperator : IOperationWithOperator
    {
        private Operator _operator;
        private IOperation _operation;

        public OperationWithOperator(IOperation _operation, Operator _operator)
        {
            this._operation = _operation;
            this._operator = _operator;
        }

        public Operator GetOperator()
        {
            return _operator;
        }

        public DateTime GetDate()
        {
            return _operation.GetDate();
        }

        public int GetDuration()
        {
            return _operation.GetDuration();
        }

        public string GetNumber()
        {
            return _operation.GetNumber();
        }

        public BillingReader.OperationType GetOperationType()
        {
            return _operation.GetOperationType();
        }
    }
}

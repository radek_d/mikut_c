﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CostCalculator;
using OperatorResolver;
using PersistenceProvider;
using BillingReader;
using System.Threading;
using MiKUTModel;


namespace CostOptimizer
{
    public class Optimizer : AbstractOptimizer
    {
        

        /**
         * PersistanceProvider reference (initiated in GUI)
         * */
        private ITariffDAO _tariffDao;

        /**
         * delegate method for GUI to update progressRate Bar
         * */
        public delegate void SetProgress(int progress);
        private SetProgress _setProgressMethod;

        /**
         * delegate method for OperatorResolver to resolve Operator by asking User
        */
        public delegate Operator ResolveOperator(string number);
        private ResolveOperator _resolveOperatorMethod;

        public Optimizer(ITariffDAO tariffDao, ResolveOperator method)
        {
            _resolveOperatorMethod = method;
            _tariffDao= tariffDao;
        }

        /**
         * delegate method for user interface module ( updating progress bar in GUI) 
         **/
        public SetProgress SetProgressMethod {
            set
            {
                _setProgressMethod = value;
            }
        }

        protected override IOperationWithOperator[] LoadBillingsAndResolveOperators(String pathToBilling)
        {
            //loading billing using billingReader
            IBillingReader billingReader = new BillingReader.BillingReader();
            IOperation[] operations = billingReader.LoadBilling(pathToBilling);
 
            //array for operations with operators
            IOperationWithOperator[] operationsWithOperators= new IOperationWithOperator[operations.Length];
        
            //operatorResolver for determing operator of operation's number 
            IOperatorResolver operatorResolver= new OperatorResolver.OperatorResolver();
           /**
            * HERE--> setting operatorResolver User Method- not yet implemented!
            **/

            for (int i = 0; i < operations.Length; i++ )
            {
                IOperation operation = operations[i]; 

                //resolving operators
                Operator _operator = operatorResolver.Resolve(operation.GetNumber());

                //adding operators to operations
                operationsWithOperators[i] = new OperationWithOperator(operation, _operator);
            }

            return operationsWithOperators;
        }

        protected override Tariff[] LoadTariffs()
        {
            Tariff[] tariffs = null; //= _tariffDao.getAllTarifes().ToArray(); ---> some errors due to test class Tariff in PersistenceProvider

            return tariffs;
        }

        protected override Tariff CalculateCostAndFindOptimal(Tariff[] tariffs, IOperationWithOperator[] billing)
        {
            // calculator for cost 
            ICalculator calculator = new CostCalculator.CostCalculator();
            
            decimal optimalCost = decimal.MaxValue;
            Tariff optimalTariff= null;

            foreach (Tariff tariff in tariffs)
            {
                decimal cost= calculator.Calculate(tariff, billing);

                if ( cost < optimalCost)
                {
                    optimalCost = cost;
                    optimalTariff = tariff;
                }
            }

            return optimalTariff;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CostCalculator;
using System.Threading;
//using PersistenceProvider;

namespace CostOptimizer
{
    public abstract class AbstractOptimizer
    {
        private int _progressRate;  //w procentach

        /***
         *  rozwiazanie nr 1 - my tworzymy watek
         * * */
        private string _optimalTariff = null; //zwraca taryfe

        public string OptimalTariffName
        {
            get
            {
                return _optimalTariff;
            }
        }

        public void startFindOptimalTariffThread(string pathToBilling)
        {
            Thread t = new Thread(() => FindOptimalTariff(pathToBilling));
            t.Start();
        }

        /***
         *  rozwiazanie nr2 - gui tworzy nasz watek
         * * */

        public string FindOptimalTariff(string pathToBilling) 
            //throws DatabaseAccessErrorException, NoRecordFoundException, ValidationErrorException, FileAccesException, InvalidFormatException
            // not yet implemented?
        {
            IOperationWithOperator[] operationsWithOperators = LoadBillingsAndResolveOperators(pathToBilling);
            Tariff[] tariffs = LoadTariffs();
            Tariff optimalTariff = CalculateCostAndFindOptimal(tariffs, operationsWithOperators);
            
            string optimalTariffName = optimalTariff.Name;
            _optimalTariff = optimalTariffName;

            return optimalTariffName;
        }



        public int ProgressRate
        {
            get
            {
                return _progressRate;
            }
        }

        //loading billing, returning list of operations with assigned operators
        protected abstract IOperationWithOperator[] LoadBillingsAndResolveOperators(String pathToBilling);

        //loads tariffs from database
        protected abstract Tariff[] LoadTariffs();

        //counts cost of every tariff and finds optimal
        protected abstract Tariff CalculateCostAndFindOptimal(Tariff[] tariff, IOperationWithOperator[] billing);
    }
}
